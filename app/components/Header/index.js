import React from 'react';
import styled from 'styled-components';
import Logo from '../../images/minsources-logo.png';

const HeaderLogo = styled.div`
  padding-left: 10%;
  @media screen and (max-width: 880px) {
    padding-left: 0;
    column-count: 1;
    text-align: center;
  }
`;

const HeaderBar = styled.div`
  column-count: 4;
  @media screen and (max-width: 880px) {
    column-count: 1;
    text-align: center;
    margin-top: -2em;
  }
`;

const HeaderMenu = styled.a`
  text-decoration: none;
  font-weight: 600;
  font-family: 'Nunito', sans-serif;
  line-height: normal;
  font-size: 15px;
  color: ${props => (props.active ? '#b4c734' : 'black')};
  text-decoration: ${props => (props.active ? 'underline' : 'none')};
  text-decoration-color: ${props => (props.active ? 'black' : 'none')};

  &:hover {
    color: #b4c734;
    text-decoration: underline;
    text-decoration-color: black;
  }
`;

const HeaderFixed = styled.div`
  width: 100%;
  position: fixed;
  @media screen and (max-width: 880px) {
    position: relative;
  }
`;

const HeaderWrap = styled.div`
  column-count: 2;
  background-color: white;
  border-bottom: 0.15em solid #d6d8db;
  @media screen and (max-width: 880px) {
    column-count: 1;
    justify-content: center;
    padding-bottom: 3%;
  }
`;

const locs = window.location.pathname;
let home = <HeaderMenu href="/">Home</HeaderMenu>;
let about = <HeaderMenu href="about">About</HeaderMenu>;
let milestone = <HeaderMenu href="milestone">Milestone</HeaderMenu>;
let sustainability = (
  <HeaderMenu href="sustainability">Sustainability</HeaderMenu>
);

if (locs === '/') {
  home = (
    <HeaderMenu active href="/">
      Home
    </HeaderMenu>
  );
} else if (locs === '/about') {
  about = (
    <HeaderMenu active href="about">
      About
    </HeaderMenu>
  );
} else if (locs === '/milestone') {
  milestone = (
    <HeaderMenu active href="milestone">
      Milestone
    </HeaderMenu>
  );
} else if (locs === '/sustainability') {
  sustainability = (
    <HeaderMenu active href="sustainability">
      Sustainability
    </HeaderMenu>
  );
}

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  render() {
    return (
      <HeaderFixed>
        <HeaderWrap>
          <HeaderLogo>
            <img src={Logo} alt="Minsources Logo" height="68px" width="140px" />
          </HeaderLogo>
          <HeaderBar style={{ paddingTop: '1.5em' }}>
            <div>{home}</div>
            <div>{about}</div>
            <div>{sustainability}</div>
            <div>{milestone}</div>
          </HeaderBar>
        </HeaderWrap>
      </HeaderFixed>
    );
  }
}

export default Header;
