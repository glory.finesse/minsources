import React from 'react';
import styled from 'styled-components';
import Logo from '../../images/minsources-logo.png';
import Facebook from '../../images/facebook.png';
import Linkedin from '../../images/linkedin.png';
import Mail from '../../images/mail.png';

const Wrapper = styled.div`
  background-color: #b7b870;
  position: relative;
  bottom: 0;
  right: 0;
  left: 0;
  @media screen and (max-width: 880px) {
    column-count: 1;
    justify-content: center;
  }
`;

const Container = styled.div`
  width: 72%;
  height: 21em;
  padding-top: 2em;
  margin-left: 14%;
  flex-direction: column;
`;

const Column = styled.div`
  display: flex;
  font-size: 0.95em;
  padding-top: 1em;
`;

const TabBranch = styled.div`
  flex-basis: 30em;
  padding-right: 1em;
`;

const TabNav = styled.div`
  flex-basis: 10em;
`;

const SocialMedia = styled.div`
  margin-top: 1.5em;
  text-align: center;
  padding-bottom: 1em;
`;

const Icon = styled.img`
  margin-left: 1.7em;
  margin-right: 1.7em;
`;

const Envelope = styled.img`
  margin-left: 1.7em;
  margin-right: 1.7em;
  margin-top: 0.3em;
`;

const Nav = styled.a`
  color: black;
  text-decoration: none;
`;

/* eslint-disable react/prefer-stateless-function */
class Footer extends React.Component {
  render() {
    return (
      <Wrapper>
        <Container>
          <img src={Logo} alt="Minsources Logo" height="68px" width="140px" />
          <Column>
            <TabBranch>
              <b>JAKARTA OFFICE</b>
            </TabBranch>
            <TabBranch>
              <b>KENDARI OFFICE</b>
            </TabBranch>
            <TabNav>
              <b>NAVIGATION</b>
            </TabNav>
          </Column>
          <Column>
            <TabBranch>
              CEO Suite, Room 1704-05 <br /> 
              Indonesia Stock Exchange Tower 2, 17th Floor <br />
              Jl. Jend. Sudirman Kav. 52-53, Jakarta 12190, Indonesia<br />
              <br />Phone: +62 21 515 7712
            </TabBranch>
            <TabBranch>
              Jalan Martandu Lorong Kharisma 3 No. 77 D <br />
              Kelurahan Kambu, Anduonohu, Kendari <br />
              Sulawesi Tenggara 93213
            </TabBranch>
            <TabNav>
              <Nav href="/">Home</Nav>
              <br />
              <Nav href="about">About</Nav>
              <br />
              <Nav href="milestone">Milestone</Nav>
              <br />
              <Nav href="sustainability">Sustainability</Nav>
            </TabNav>
          </Column>
          <SocialMedia>
            <Icon
              src={Facebook}
              alt="Facebook Logo"
              height="25px"
              width="25px"
            />
            <Icon
              src={Linkedin}
              alt="Facebook Logo"
              height="25px"
              width="25px"
            />
            <Envelope
              src={Mail}
              alt="Facebook Logo"
              height="25px"
              width="25px"
            />
          </SocialMedia>
        </Container>
      </Wrapper>
    );
  }
}

export default Footer;
