import React from 'react';
import styled from 'styled-components';
import MissionPicture from '../../images/mission.jpg';
import VisionPicture from '../../images/stone.jpg';
import AboutUsPicture from '../../images/banner-about-us.png';
import ProductivityPicture from '../../images/productivity.png';
import BestServicePicture from '../../images/best-service.png';
import ChancesPicture from '../../images/chances.png';
import NetworkPicture from '../../images/network.png';
import ImprovementPicture from '../../images/improvement.png';
import IntegrityPicture from '../../images/integrity.png';

const ContainerWrapper = styled.div`
  width: 70%;
  margin-left: 15%;
  margin-bottom: 5em;
`;

const PageTitleWrapper = styled.div`
  width: 70%;
  margin-left: 15%;
  @media screen and (max-width: 880px) {
    margin-top: -5em;
  }
`;

const Title = styled.h1`
  font-size: 2.5em;
  color: #57791e;
  font-weight: 800;
  font-family: 'Nunito', sans-serif;
`;

const Subtitle = styled.h2`
  color: #b7b870;
  font-size: 1.5em;
  margin-top: -1.1em;
  margin-bottom: 1.5em;
  font-weight: 600;
  font-family: 'Nunito', sans-serif;
`;

const PageBanner = styled.img`
  left: -25%;
  width: 100%;
  height: 20em;
`;

const SectionTitle = styled.h2`
  margin-top: 2%;
  color: black;
  font-size: 2.2em;
  text-align: center;
  padding-top: 1%;
  font-family: 'Source Serif Pro', serif;
  font-weight: 700;
`;

const Section = styled.div`
  text-align: justify;
  column-count: 2;
  font-size: 0.95em;
  line-height: 1.25em;
  padding-bottom: 5%;
  padding-top: 5%;
  border-bottom: 0.05em solid #a2a3a5;
  font-weight: 400;
  font-family: 'Nunito', sans-serif;
  @media screen and (max-width: 880px) {
    column-count: 1;
    column-gap: 0;
    justify-content: center;
    padding-bottom: 7%;
  }
`;

const RightCaption = styled.div`
  text-decoration: none;
  font-style: italic;
  font-weight: 400;
  font-family: 'Nunito', sans-serif;
  line-height: 1.5em;
  font-size: 1.3em;
  text-align: center;
  color: #636466;
  padding-left: 15%;
  padding-right: 15%;
`;

const LeftCaption = styled.ul`
  text-align: left;
  line-height: 1.5em;
  font-weight: 400;
  font-family: 'Nunito', sans-serif;
`;

const Picture = styled.img`
  margin-bottom: 2em;
`;

const Values = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`;

const ValueTab = styled.div`
  text-align: center;
  padding-bottom: 7em;
  width: 18.5em;
  height: 20em;
`;

const ValueTitle = styled.div`
  padding-top: 0.3em;
  padding-bottom: 0.3em;
  text-align: center;
  font-size: 1.5em;
  font-family: 'Crimson Text', serif;
  font-weight: bold;
`;

const ValueCaption = styled.div`
  text-align: center;
  font-size: 0.9em;
  padding-left: 5%;
  padding-right: 5%;
`;

export default function About() {
  return (
    <div>
      {/* Page Title */}
      <PageTitleWrapper>
        <Title>PT Minsources International</Title>
        <Subtitle>A Company with Quality First Vision</Subtitle>
      </PageTitleWrapper>

      {/* Page Banner */}
      <PageBanner src={AboutUsPicture} alt="About Us Banner" />

      <ContainerWrapper>
        {/* About Us */}
        <SectionTitle>About Us</SectionTitle>
        <Section style={{ marginTop: '-5%', columnGap: '10%' }}>
          PT Minsources International is a national private organization
          established on 16th of July, 2008 engaged in the export trade business
          of nickel ore mining products. Partnering with PT. Aneka Tambang Tbk,
          we exported 2.195.035 WMT to China from year 2008 until 2010. In 2011,
          PT Minsources International together with PT. Aneka Tambang Tbk has
          signed 600.000 WMT purchased contracts.
          <br />
          <br />
          With full assurance by overviewing the great prospect of minerals and
          mining market together with actively taking parts in regional
          development, PT Minsource International devotes high capabilities and
          full dedication in building organization that focuses on mining and
          its product sales.
          <br />
          <br />
          PT Minsource International continues to strive for improving quality
          and quantity of production in order to achieve and gain customer
          satisfaction both internally and externally.
          <br />
          <br />
          The main foundation that keep PT Minsource International to continue
          in developing in the field of mineral mining along with refining
          processing facilities is the valuable increasing trust rate.
        </Section>

        {/* Vision */}
        <Section style={{ paddingLeft: '10%', paddingRight: '10%' }}>
          <Picture
            src={VisionPicture}
            alt="Vision Picture"
            width="100%"
            height="200em"
          />
          <SectionTitle>
            <br />Vision
          </SectionTitle>
          <RightCaption>
            "Become a leading supplier in mineral processing and refining
            industy in Indonesia."
          </RightCaption>
        </Section>

        {/* Mission */}
        <Section
          style={{ paddingLeft: '2%', paddingRight: '5%', columnGap: '5%' }}
        >
          <SectionTitle>Mission</SectionTitle>
          <LeftCaption>
            <li>
              Become a leading supplier for mineral processing and refining
              industry with high efficiency
            </li>
            <br />
            <li>Fully committed with safety aspects of each operation</li>
            <br />
            <li>
              Putting forward development for sustainable supply by maintaining
              the environmental conservasions
            </li>
            <br />
          </LeftCaption>
          <Picture
            src={MissionPicture}
            alt="Mission Picture"
            height="270em"
            width="100%"
          />
        </Section>

        {/* Company Values  */}
        <SectionTitle>Company Values</SectionTitle>
        <Values style={{ paddingBottom: '5%' }}>
          <ValueTab>
            <img width="150em" height="150em" src={NetworkPicture} />
            <ValueTitle>Network</ValueTitle>
            <ValueCaption>
              We provide wide network by having Supporting Office as
              representatives of our Head Office on each working areas.
            </ValueCaption>
          </ValueTab>
          <ValueTab>
            <img width="150em" height="150em" src={ImprovementPicture} />
            <ValueTitle>Improvement</ValueTitle>
            <ValueCaption>
              Constantly learn new things and make improvements for work
              effectiveness and cost efficiency.
            </ValueCaption>
          </ValueTab>
          <ValueTab>
            <img width="150em" height="150em" src={ProductivityPicture} />
            <ValueTitle>Productivity</ValueTitle>
            <ValueCaption>
              Constantly maintain the high quality of resources shared within
              the organization to produce superior products and services.
            </ValueCaption>
          </ValueTab>
          <ValueTab>
            <img width="150em" height="150em" src={IntegrityPicture} />
            <ValueTitle>Integrity</ValueTitle>
            <ValueCaption>
              Concept of thinking and acting for delivering each of our service
              are: Honest, Open, and Trustworthy.
            </ValueCaption>
          </ValueTab>
          <ValueTab>
            <img width="150em" height="150em" src={BestServicePicture} />
            <ValueTitle>Best Service</ValueTitle>
            <ValueCaption>
              Each individuals are working together to achieve something better,
              striving to provide best services beyond our customer
              expectations.
            </ValueCaption>
          </ValueTab>
          <ValueTab>
            <img width="150em" height="150em" src={ChancesPicture} />
            <ValueTitle>Chances & Opportunities</ValueTitle>
            <ValueCaption>
              Provide opportunities with authority based on capacity and their
              ability to become great leaders in the fields they are expertise
              in by creating sense of ownership and high responsibilities.
            </ValueCaption>
          </ValueTab>
        </Values>
      </ContainerWrapper>
    </div>
  );
}
