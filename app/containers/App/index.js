import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import Header from 'components/Header';
import Footer from 'components/Footer';
import About from 'containers/About/Loadable';
import Home from 'containers/Home/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Sustainability from '../Sustainability';
import Milestone from '../Milestone';

const AppWrapper = styled.div`
  display: flex;
  padding-top: 5.5em;
  min-height: 30em;
  flex-direction: column;
`;

export default function App() {
  return (
    <div>
      <Header />
      <AppWrapper>
        <Helmet
          titleTemplate="%s | PT Minsources International"
          defaultTitle="PT Minsources International"
        >
          <meta name="description" content="PT Minsources International" />
        </Helmet>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/sustainability" component={Sustainability} />
          <Route exact path="/milestone" component={Milestone} />
          <Route path="" component={NotFoundPage} />
        </Switch>
      </AppWrapper>
      <Footer />
    </div>
  );
}
