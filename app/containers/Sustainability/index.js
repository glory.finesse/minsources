import React from 'react';
import styled from 'styled-components';
import SustainabilityPicture from '../../images/banner-sustainability.png';
import EducationPicture from '../../images/education.jpg';
import HealthPicture from '../../images/health.jpg';
import EconomicPicture from '../../images/economic.jpg';
import InfrastructurePicture from '../../images/infrastructure.jpg';
import LivingPicture from '../../images/living.jpg';

const ContainerWrapper = styled.div`
  width: 70%;
  margin-left: 15%;
`;

const PageTitleWrapper = styled.div`
  width: 70%;
  margin-left: 15%;
  @media screen and (max-width: 880px) {
    margin-top: -5em;
  }
`;

const Title = styled.h1`
  font-size: 2.5em;
  color: #57791e;
  font-weight: 800;
  font-family: 'Nunito', sans-serif;
`;

const Subtitle = styled.h2`
  color: #b7b870;
  font-size: 1.5em;
  font-weight: 600;
  font-family: 'Nunito', sans-serif;
  margin-top: -1.1em;
  margin-bottom: 1.5em;
`;

const PageBanner = styled.img`
  left: -25%;
  width: 100%;
  height: 20em;
`;

const SectionTitle = styled.h2`
  margin-top: 2%;
  color: black;
  font-size: 1.8em;
  text-align: left;
  padding-top: 1%;
  font-family: 'Source Serif Pro', serif;
  font-weight: 700;
`;

const Section = styled.div`
  text-align: justify;
  column-count: 2;
  font-size: 0.95em;
  line-height: 1.5em;
  padding-bottom: 5%;
  padding-top: 5%;
  border-bottom: 0.05em solid #a2a3a5;
  font-weight: 400;
  font-family: 'Nunito', sans-serif;
  @media screen and (max-width: 880px) {
    column-count: 1;
    column-gap: 0;
    justify-content: center;
    padding-bottom: 7%;
    padding-right: 7%;
    padding-left: 5%;
  }
`;

const SpecialSection = styled.div`
  column-count: 2;
  font-size: 0.95em;
  line-height: 1.5em;
  padding-bottom: 5%;
  padding-top: 5%;
  border-bottom: 0.05em solid #a2a3a5;
  font-weight: 400;
  font-family: 'Nunito', sans-serif;
  @media screen and (max-width: 995px) {
    column-count: 1;
    column-gap: 0;
    justify-content: center;
    padding-bottom: 7%;
    padding-right: 7%;
    padding-left: 5%;
  }
`;

const Caption = styled.div`
  text-align: left;
  line-height: 1.5em;
  padding-right: 5%;
  font-weight: 400;
  font-family: 'Nunito', sans-serif;
  padding-bottom: 2%;
  font-size: 1em;
  @media screen and (max-width: 995px) {
    text-align: justify;
  }
`;

const Picture = styled.img`
  padding-bottom: 2%;
  margin-left: 10%;
  @media screen and (max-width: 880px) {
    margin-left: 10%;
    width: 80%;
    height: 80%;
  }
`;

const SpecialPicture = styled.img`
  padding-bottom: 2%;
  margin-left: 20%;
  @media screen and (max-width: 995px) {
    margin-left: 10%;
    width: 80%;
    height: 80%;
  }
`;

const SectionWrapper = styled.div`
  padding-left: 5%;
`;

export default function About() {
  return (
    <div>
      {/* Page Title */}
      <PageTitleWrapper>
        <Title>PT Minsources International</Title>
        <Subtitle>A Company with Quality First Vision</Subtitle>
      </PageTitleWrapper>

      {/* Page Banner */}
      <PageBanner src={SustainabilityPicture} alt="About Us Banner" />

      <ContainerWrapper>
        {/* Sustainability */}
        <SectionTitle style={{ textAlign: 'center' }}>
          Sustainability
        </SectionTitle>
        <Section style={{ marginTop: '-5%', columnGap: '10%' }}>
          Our organization believes that sustainable development is one of the
          key to sustainable business and long term sustainable growth. Our
          organization Social Responsibility Program aims to help improve the
          welfare of the local community through various programs covering 4
          (four) pillars in Education, Health, Infrastructure and Economic
          Empowerment. To accomplish these objectives, our organization
          continues to implement and commit to various corporate social
          responsibility activities together with our values and in harmony with
          the interests of stakeholders.
        </Section>

        {/* Education */}
        <Section>
          <Picture
            src={EducationPicture}
            alt="Education Picture"
            width="80%"
            height="270em"
          />
          <SectionWrapper>
            <SectionTitle>
              <br />Education
            </SectionTitle>
            <Caption>
              Through CSR activities, our organization strives to help create
              high quality moral education, which unifies science, creativity,
              innovation and character. These activities include the provision
              of scholarships and the construction of public park for readings.
            </Caption>
          </SectionWrapper>
        </Section>

        {/* Health */}
        <Section>
          <SectionWrapper>
            <SectionTitle>Health</SectionTitle>
            <Caption>
              CSR activities aim to improve the quality of public health by
              increasing the capacity of Health Clinics (Pos Pelayanan Terpadu)
              for family health. Our organization also prioritizes the provision
              of information and raising awareness about clean and healthy
              lifestyle, as well as spreading humanity to whole communities.<br />
              <br />
            </Caption>
          </SectionWrapper>
          <Picture
            src={HealthPicture}
            alt="Health Picture"
            width="80%"
            height="270em"
          />
        </Section>

        {/* Economic Empowerment */}
        <SpecialSection>
          <Picture
            src={EconomicPicture}
            alt="Economic Picture"
            width="80%"
            height="270em"
          />
          <SectionWrapper>
            <SectionTitle>
              <br />Economic Empowerment
            </SectionTitle>
            <Caption>
              One of the key for our organization’s CSR is to empower the
              economy by encouraging local economic strengthening initiatives
              through optimizing local potential-based business groups. This
              action also provide intensive assistance to strengthen
              institutional business groups and improve the quality of human
              resources, and introduce a healthy business competition
              environment.<br />
            </Caption>
          </SectionWrapper>
        </SpecialSection>

        {/* Infrastructure */}
        <Section>
          <SectionWrapper>
            <SectionTitle>Infrastructure</SectionTitle>
            <Caption>
              Infrastructure development program involves the active role of
              Community Communication Forum in deliberations of relevant
              parties, which are districts, sub-districts, and village
              government. Our organization committed to provide financial
              support for the implementation of infrastructure development, as
              well as participate in planning, implementation, monitoring and
              evaluation of development.<br />
              <br />
            </Caption>
          </SectionWrapper>
          <Picture
            src={InfrastructurePicture}
            alt="Infrastructure Picture"
            width="80%"
            height="270em"
          />
        </Section>

        {/* Living Environment */}
        <Section style={{ borderBottom: 'none', columnCount: '1' }}>
          <SpecialPicture
            src={LivingPicture}
            alt="Living Picture"
            width="60%"
            height="300em"
          />
          <SectionWrapper>
            <SectionTitle
              style={{ marginTop: '-2%', textAlign: 'left', fontSize: '2em' }}
            >
              <br />Living Environment
            </SectionTitle>
            <Caption style={{ textAlign: 'justify' }}>
              Our organization strongly aware that environmental issues is one
              of the most significant in conducting our mining business
              activities. Our organization always make sure to take all actions
              to ensure the safety and preservation of the surrounding
              environment. Facilities and equipments are selected based on
              industry standards for occupational health and safety and will be
              used and maintained periodically in accordance with standard
              operating procedures and manufacturing instructions. We believe by
              implementing good mining practices surely will reduce all the
              negative impacts.
            </Caption>
          </SectionWrapper>
        </Section>
      </ContainerWrapper>
    </div>
  );
}
