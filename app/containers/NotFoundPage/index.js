/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const AppWrapper = styled.div`
  width: 70%;
  margin-left: 15%;
  font-weight: 800;
  font-family: 'Nunito', sans-serif;
`;

export default function NotFound() {
  return (
    <AppWrapper>
      <h1>
        <FormattedMessage {...messages.header} />
      </h1>
    </AppWrapper>
  );
}
