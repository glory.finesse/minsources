import React from 'react';
import styled from 'styled-components';
import { Slide } from 'react-slideshow-image';
import Home1 from '../../images/home1.png';
import Home2 from '../../images/banner-about-us.png';
import Home3 from '../../images/banner-sustainability.png';

const PageTitleWrapper = styled.div`
  width: 70%;
  margin-left: 15%;
  @media screen and (max-width: 880px) {
    margin-top: -5em;
  }
`;

const Title = styled.h1`
  font-size: 2.5em;
  color: #57791e;
  font-weight: 800;
  font-family: 'Nunito', sans-serif;
  text-align: center;
`;

const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrows: true,
};

export class Home extends React.Component {
  render() {
    return (
      <div>
        <div>
          <PageTitleWrapper>
            <Title>PT Minsources International</Title>
          </PageTitleWrapper>
        </div>
        <div style={{ backgroundColor: '#b7b870' }}>
          <Slide {...properties}>
            <div className="each-slide">
              <img src={Home1} width="100%" height="400px" />
            </div>
            <div className="each-slide">
              <img src={Home3} width="100%" height="400px" />
            </div>
            <div className="each-slide">
              <img src={Home1} width="100%" height="400px" />
            </div>
          </Slide>
        </div>
      </div>
    );
  }
}

export default Home;
