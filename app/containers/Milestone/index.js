import React from 'react';
import styled from 'styled-components';
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import MilestoneBanner from '../../images/banner-milestone.jpg';
import Picture1 from '../../images/1.jpg';
import Picture2 from '../../images/2.jpg';
import Picture3 from '../../images/3.jpg';
import Picture4 from '../../images/4.jpg';
import Picture5 from '../../images/5.jpg';
import Picture6 from '../../images/6.jpg';
import Picture7 from '../../images/7.jpg';
import Picture8 from '../../images/8.jpg';

const Title = styled.h2`
  color: #b7b870;
  font-size: 1.5em;
  font-weight: 600;
  font-family: 'Nunito', sans-serif;
  text-align: center;
  margin-top: 3%;
`;

const Subitle = styled.h2`
  color: #b7b870;
  font-size: 1em;
  margin-top: -1.1em;
  margin-bottom: 1.5em;
  font-weight: 400;
  font-family: 'Nunito', sans-serif;
  text-align: center;
`;

const Pic = styled.img`
  border-radius: 50%;
  width: 100%;
  height: 100%;
`;

export class Milestone extends React.Component {
  render() {
    return (
      <div style={{ backgroundColor: 'black' }}>
        <img
          src={MilestoneBanner}
          alt="Milestone Banner"
          width="100%"
          height="500em"
          style={{ marginTop: '-10em' }}
        />
        <Title>
          <b>OVER 10 YEARS</b>
        </Title>
        <Subitle>We build this company from zero until today</Subitle>
        <VerticalTimeline>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            icon={<Pic src={Picture8} />}
          >
            <h3 className="vertical-timeline-element-title">2018</h3>
            <p>
              Until the end of February, the organization has supplied 160,000
              WMT to nickel smelting and refineries.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            icon={<Pic src={Picture7} />}
          >
            <h3 className="vertical-timeline-element-title">2017</h3>
            <p>
              Began supplying 410,000 WMT of nickel ore to domestic smelting and
              nickel smelting companies. Acquired several Nickel Mines IUP to
              increase supply.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            icon={<Pic src={Picture6} />}
          >
            <h3 className="vertical-timeline-element-title">2016</h3>
            <p>
              By the end of November 2016, restarted the buying and selling
              activities of nickel ore.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            icon={<Pic src={Picture5} />}
          >
            <h3 className="vertical-timeline-element-title">2014</h3>
            <p>
              Enactment of Minister of Energy and Mineral Resources Regulation
              1/2014 concerning Prohibition of Raw Mineral Exports, so that the
              organization and related industries in the country temporarily
              stop all its business activities.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            icon={<Pic src={Picture4} />}
          >
            <h3 className="vertical-timeline-element-title">2011</h3>
            <p>
              Iron ore mining in Serongga, South Kalimantan. Together with PT.
              Aneka Tambang Tbk signed 600.000 WMT purchased contracts.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            icon={<Pic src={Picture3} />}
          >
            <h3 className="vertical-timeline-element-title">2010</h3>
            <p>
              Cooperation of nickel ore mine site management in Tinanggea,
              Konaweatan, Southeast Sulawesi with PT Baula Petra Buana.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            icon={<Pic src={Picture2} />}
          >
            <h3 className="vertical-timeline-element-title">2009</h3>
            <p>
              Iron ore mining in Lhoong, Aceh Besar, Nangroe Aceh Darussalam
              with total production of 50,000 MT per month.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            icon={<Pic src={Picture1} />}
          >
            <h3 className="vertical-timeline-element-title">2008</h3>
            <p>
              Establishment of PT Minsource International which is engaged in
              the sale of mineral mining products. Partnering with PT. Aneka
              Tambang Tbk, exported 2.195.035 WMT to China until year 2010.
            </p>
          </VerticalTimelineElement>
        </VerticalTimeline>
      </div>
    );
  }
}

export default Milestone;
